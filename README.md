# React js CRUD with Asp.Net Web API

React js CRUD with Asp.Net Web API.


Contents discussed :
Asp.Net Web API
 - Project Creation
 - Define DB Model
 - Add Foreign Key in EF Core
 - DB Migration
 - Create API Controller with CRUD Web Methods
 - Make Post Req. / Insert an order
 - Retrieve/ Fetch Existing Records
 - Delete an Order
 
React.js App.
 - Create React js App. & App Structure.
 - Install Material UI.
 - Design a Form for Master (Order Summary).
 - Populate Customer DropDown with the Collection from the API.
 - Make HTTP Request to Web API from React JS & Enable CORS.
 - Add Component to list and search all food items.
 - Select Food Items to the Order.
 - Create Component to List Select Food Items and managing them.
 - Calculate Order Grand Total.
 - Form Validation.
 - Create an Order or Submit an Order.
 - Retrieve and Display existing Order.
 - Update an entire Order.
 - Notification Component.
 - Delete an entire Order.

import logo from './logo.svg';
import './App.css';
import { Container, Typography } from "@material-ui/core";
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Order from "./components/Order";
import Header from './components/Header'
import Footer from './components/Footer'
import About from './components/About'
import Services from './components/Services'
function App() {
  return (
    <Router>
      <Header />
      <main className='py-3'>
        <Container maxWidth="md">
          <Order />
        </Container>
      </main>
      <Footer />
    </Router>
  );
}

export default App;
